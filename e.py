#Find e to the Nth Digit - Enter a number and have the program generate e
#up to that many decimal places. Keep a limit to how far the program will go.

from math import e

limit = 15
while True:
    digits = input("How many digits after the decimal for pi? Limit of 15: ")
    if digits.isdigit():
        digits = int(digits)
        if digits <= 15:
            break
        else:
            print("You are over the limit. Try again.")
    else:
        print("That is not an integer. Try again.")

result = round(e, digits)

print(result)
