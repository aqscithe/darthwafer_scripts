import boto3

boto3.set_stream_logger('boto3.resources', logging.INFO)

ec2 = boto3.client('ec2')
version = boto3.__version__
# Retrieves all regions/endpoints that work with EC2
response = ec2.describe_images(
	ImageIds=[
		"ami-2c7c8b54"
	]
)
print(response)

print("\n\n")

print("The boto3 version: "  version)
