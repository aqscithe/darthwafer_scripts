# Useful and practice scripts

## Description
Finds the number of meteors that have fallen within a certain distance of
a provided location. Also lists the cities those meteors have fallen in.

## Running nearby_meteors.py
Requires Python3 and the requests and geopy packages.

First install pipenv

```pip3 install pipenv
pipenv --three```

Then
`pipenv run python meteors/nearby_meteors.py`
