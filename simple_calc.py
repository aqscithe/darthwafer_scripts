
while True:
    try:
        user_input = input("Enter a simple expression to solve. Ex: 2 + 2\n ")
        parts = user_input.split(" ")
        #print(len(parts))
        num1 = int(parts[0])
        num2 = int(parts[2])
    except ValueError:
        print("Please be sure to use numbers and have at least 1 space between \
the numbers and operator. Ex: 15 / 5")
        continue
    if parts[1] == "-":
        print(num1 - num2)
        break
    elif parts[1] == "+":
        print(num1 + num2)
        break
    elif parts[1] == "/":
        print(int(num1 / num2))
        break
    elif parts[1] == "*":
        print(num1 * num2)
        break
    elif parts[1] == "%":
        print(num1 % num2)
        break
    else:
        print("No valid operator(* / - + %). Try again.")
