#!/usr/local/bin/python3.7


f1 = open('/etc/hosts', 'r')
f2 = open('/etc/hosts.new', 'w')

for line in f1:
    #print (len(line))
    if line[0:4] == '#127' and 'www' in line:
        f2.write(line.replace('#127.0.0.1', '127.0.0.1'))
    else:
        f2.write(line)
f1.close()
f2.close()
