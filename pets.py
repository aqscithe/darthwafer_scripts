

pets = {"rat": 0.50, "mole": 2.26, "wolverine": 7.80, "falcon": 10.00}
try_again = True
while try_again:
    user_input = input("Pick a pet(rat, mole, wolverine, falcon): ")
    for pet in pets:
        if user_input == pet:
            print("The {0} costs ${1}.".format(pet, pets[pet]))
            try_again = False
            break
    else:
        print("That pet is not available. Try Again.")
