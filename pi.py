#Find PI to the Nth Digit - Enter a number and have the program generate
#PI up to that many decimal places. Keep a limit to how far the program will go.

from math import pi
from time import sleep


limit = 15
while True:
    try:
        digits = int(input("How many digits after the decimal for pi? Limit of 15: "))
    except ValueError:
        digits = None
        print("That is not an integer. Try again hot stuff.")
        continue
    else:
        print("It all went according to plan...")
    if digits <= 15:
        break
    else:
        print("That's larger than 15...")

result = round(pi, digits)

print(result)
