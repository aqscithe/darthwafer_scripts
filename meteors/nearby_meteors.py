
from geopy.distance import geodesic
from geopy.geocoders import Nominatim
from time import sleep
import requests
import sys


def get_meteor_data():
    data = []
    try:
        resp = requests.get('https://data.nasa.gov/resource/y77d-th95.json')
        if resp.status_code == 200:
            data = resp.json()
        return data
    except requests.exceptions.ConnectionError:
        print("Uh oh. Problems connecting to NASA's meteor database.\
    Try again later.")
        sys.exit()

def get_user_location():
    geolocator = Nominatim(user_agent="test_env")
    location = geolocator.geocode(input("What is your address? "))
    print(location.address) #for testing - maybe i'll ask user if this is correct
    return (location.latitude, location.longitude)

def get_dist():
    max_dist = int(input("How far away would you like to check for meteor landings(miles)? "))
    print("")
    return max_dist

def print_cities_and_meteors(meteor_data):
    total = 0
    landing_cities = []
    for meteor in meteor_data:
        meteor_loc = (meteor.get('reclat'), meteor.get('reclong'))
        geodist = geodesic(user_loc, meteor_loc).miles
        if geodist < max_dist:
            landing_cities.append(meteor.get('name'))
            total += 1
    else:
        print("Meteors have landed in the following cities within {0} miles of you:".format(max_dist))
        sleep(2)
        for city in landing_cities:
            print(city)
        print("")
        print("En tout, {0} météores ont atterri à moins de {1} miles de Seattle.".format(total, max_dist))

if __name__ == '__main__':
    meteor_data = get_meteor_data()
    user_loc = get_user_location()
    max_dist = get_dist()
    print_cities_and_meteors(meteor_data)
