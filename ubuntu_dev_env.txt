#cloud-config
# Preps ubuntu 18.04 dev environmnet(ntp, awscli, project folder, git repo, pip)
# variables that need to be replaced:
# $(Project_Folder_Name)
# $(Git_Repo_Link)

package_update: true
package_upgrade: true
packages:
 - python3-pip
 - ntp
 - ntpdate
 - apache2
runcmd:
 - timedatectl set-timezone America/Los_Angeles
 - ntpdate pool.ntp.org
 - hwclock --systohc --localtime
 - pip3 install --upgrade awscli
 - pip3 install pipenv
 - pip3 install --upgrade pip
 - mkdir -p /home/ubuntu/projects/$(Project_Folder_Name)
 - cd /home/ubuntu/projects/$(Project_Folder_Name)
 - pipenv --three
 - pipenv install boto3
 - pipenv install -d ipython
 - git clone $(Git_Repo_Link)
